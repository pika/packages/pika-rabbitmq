# Install pika-rabbitmq

```sh
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-rabbitmq.git
```

You need to adjust the settings and credentials in the file `docker-compose.yml`.

```sh
vi docker-compose.yml
...
    environment:
      RABBITMQ_ERLANG_COOKIE: secretcookie
      RABBITMQ_DEFAULT_USER: pika
      RABBITMQ_DEFAULT_PASS: pika123
      RABBITMQ_DEFAULT_VHOST: test
```

RabbitMQ uses the so-called [Erlang cookie](https://www.rabbitmq.com/docs/clustering#erlang-cookie) for authentication.
The cookie is just a string of alphanumeric characters up to 255 characters in size.

Install the container for pika-rabbitmq.

```sh
./install.sh
```
